# Dz02_05

Potrebno je napisati klasu `CyrillicTransliterateImpl` koja implementira interfejs `ICyrillicTransliterate`.  
```java
public interface ICyrillicTransliterate {

	public String transliterate(String cyrilicText);
}
```

## Uraditi
Metoda `public String transliterate(String cyrilicText)` treba da prihvati string koji sadrži ćirilične karaktere.  
Kao povratnu vrednost treba da vrati string koji sadrži latinične karaktere.  

Osmisliti algoritam za transkribovanje iz jednog pisma u drugo.  
U implementaciji dodati sve neophodne metod i promenljive (da budu privatne).

## Pretpostavka
Ćirilični tekst sadrži isključivo mala i velika ćirilična slova i znak razmaka.

## Važna napomena
Voditi računa o količni zauzetog memorijskog prostora.  
Prilikom čuvanja u memoriji Stringovi su nepromenljivi objekti.
