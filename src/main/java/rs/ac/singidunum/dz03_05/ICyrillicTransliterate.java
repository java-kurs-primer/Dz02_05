package rs.ac.singidunum.dz03_05;

public interface ICyrillicTransliterate {

	public String transliterate(String cyrilicText);

}
