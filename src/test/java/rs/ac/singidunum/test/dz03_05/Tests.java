package rs.ac.singidunum.test.dz03_05;

import org.junit.Assert;
import org.junit.Test;
import rs.ac.singidunum.dz03_05.CyrillicTransliterateImpl;
import rs.ac.singidunum.dz03_05.ICyrillicTransliterate;

public class Tests {
  
  private final ICyrillicTransliterate ct = new CyrillicTransliterateImpl();
  
  @Test
  public void test() {
    final String expected = "Praktikum Programski sistemi";
    final String val = ct.transliterate("Практикум Програмски системи");
    Assert.assertEquals(expected, val);
  }
  
  @Test
  public void test01() {
    final String epected = "";
    final String val = this.ct.transliterate("");

    Assert.assertEquals(epected, val);
  }
  
}
